import mongoose from 'mongoose';

import serverConfig from '../../config';
import CPFs from '../models/cpfSchema';

mongoose.connect(`${serverConfig.mongodb}`, { useNewUrlParser: true });

export const criaUm = cpf =>
  new Promise((resolve, reject) => {
    CPFs.findOne({ cpf }, (erro, encontrado) => {
      if (!erro && !encontrado) {
        const instancia = new CPFs({ cpf });
        resolve(instancia.save());
      } else if (encontrado) {
        resolve('jaexiste');
      } else {
        reject(new Error(null));
      }
    });
  });

export const leUm = cpf => CPFs.findOne({ cpf });

export const removeUm = cpf => CPFs.findOneAndDelete({ cpf });

export const leTotal = () => CPFs.find().estimatedDocumentCount();
