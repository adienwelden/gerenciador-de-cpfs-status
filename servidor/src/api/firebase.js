import * as admin from 'firebase-admin';

import serverConfig from '../../config';
import serviceAccount from '../../firebaseKeys.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: serverConfig.firebaseDatabaseURL,
  storageBucket: serverConfig.firebaseStorageBucket,
});

const cpfRef = admin.database().ref(serverConfig.firebaseDatabaseName);

export const criaUm = cpf =>
  cpfRef
    .orderByChild('cpf')
    .equalTo(cpf)
    .once('value')
    .then(snapshot => {
      let res;
      if (snapshot.val()) {
        res = 'jaexiste';
      } else {
        const newNode = cpfRef.push();
        newNode.set({ cpf });
        res = { cpf };
      }
      return res;
    })
    .catch(() => null);

export const leUm = cpf =>
  cpfRef
    .orderByChild('cpf')
    .equalTo(cpf)
    .once('value')
    .then(snapshot => {
      let res;
      const cpfs = [];
      if (snapshot.val()) {
        snapshot.forEach(item => cpfs.push(item.val()));
        res = cpfs.shift();
      } else {
        res = null;
      }
      return res;
    })
    .catch(() => null);

export const removeUm = cpf =>
  cpfRef
    .orderByChild('cpf')
    .equalTo(cpf)
    .once('value')
    .then(snapshot => {
      let res;
      const cpfs = [];
      if (snapshot.val()) {
        snapshot.forEach(item => {
          cpfs.push(item.val());
          cpfRef.child(item.key).remove();
        });
        res = cpfs.shift();
      } else {
        res = null;
      }
      return res;
    })
    .catch(() => null);

export const leTotal = () =>
  cpfRef
    .once('value')
    .then(snapshot => {
      let numcpfs = 0;
      if (snapshot.val()) {
        snapshot.forEach(() => {
          numcpfs += 1;
        });
      }
      return numcpfs;
    })
    .catch(() => null);
