import { Router } from 'express';

import * as cpfController from '../controllers/cpf';

const router = new Router();

/*-------------------*/

router.route('/').post(cpfController.insere);

router.route('/:cpf').get(cpfController.verifica);

router.route('/:cpf').delete(cpfController.remove);

router.route('/server/status').get(cpfController.serverStatus);

/*-------------------*/

export default router;
