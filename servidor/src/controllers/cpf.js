import CPF from 'cpf-check';

import * as api from '../api/mongodb'; // ( usar ou mongodb ou firebase )

let totalConsultas = 0;

const cpfValido = cpf => {
  const validar = CPF.strip(cpf);
  const resultado = CPF.validate(validar);
  return resultado.valid;
};

const formataUptime = seconds => {
  const secNum = parseInt(seconds, 10);
  let hours = Math.floor(secNum / 3600);
  let minutes = Math.floor((secNum - hours * 3600) / 60);
  let secnds = secNum - hours * 3600 - minutes * 60;

  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (secnds < 10) {
    secnds = `0${secnds}`;
  }
  const time = `${hours}:${minutes}:${secnds}`;
  return time;
};

export const insere = (req, res) => {
  const { cpf } = req.body;
  const response = { ok: false, payload: null };

  if (!cpfValido(cpf)) {
    response.ok = true;
    response.payload = {
      cpf,
      msg: 'CPF inválido',
    };
    res.status(200).json(response);
    return;
  }

  api
    .criaUm(cpf)
    .then(result => {
      if (result && result.cpf && result.cpf === cpf) {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'inserido com sucesso',
        };
        res.status(201).json(response);
      } else if (result === 'jaexiste') {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'já existe na lista suja',
        };
        res.status(200).json(response);
      } else {
        response.payload = 'Ocorreu uma falha no servidor.';
        res.status(500).json(response);
      }
    })
    .catch(() => {
      response.payload = 'Ocorreu uma falha no servidor.';
      res.status(500).json(response);
    });
};

export const verifica = (req, res) => {
  const { cpf } = req.params;
  const response = { ok: false, payload: null };

  if (!cpfValido(cpf)) {
    response.ok = true;
    response.payload = {
      cpf,
      msg: 'CPF inválido',
    };
    res.status(200).json(response);
    return;
  }

  api
    .leUm(cpf)
    .then(result => {
      totalConsultas += 1;
      if (result && result.cpf && result.cpf === cpf) {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'BLOCK',
        };
        res.status(200).json(response);
      } else {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'FREE',
        };
        res.status(200).json(response);
      }
    })
    .catch(() => {
      response.payload = 'Ocorreu uma falha no servidor.';
      res.status(500).json(response);
    });
};

export const remove = (req, res) => {
  const { cpf } = req.params;
  const response = { ok: false, payload: null };

  if (!cpfValido(cpf)) {
    response.ok = true;
    response.payload = {
      cpf,
      msg: 'CPF inválido',
    };
    res.status(200).json(response);
    return;
  }

  api
    .removeUm(cpf)
    .then(result => {
      if (result && result.cpf && result.cpf === cpf) {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'removido com sucesso.',
        };
        res.status(200).json(response);
      } else {
        response.ok = true;
        response.payload = {
          cpf,
          msg: 'NÃO existe na lista suja para que possa ser removido.',
        };
        res.status(200).json(response);
      }
    })
    .catch(() => {
      response.payload = 'Ocorreu uma falha no servidor.';
      res.status(500).json(response);
    });
};

export const serverStatus = (req, res) => {
  const response = { ok: false, payload: null };
  let totalCPF;

  api
    .leTotal()
    .then(result => {
      if (parseInt(result, 10) >= 0) {
        totalCPF = result;
      } else {
        totalCPF = -1;
      }
    })
    .then(() => {
      response.ok = true;
      response.payload = {
        uptime: formataUptime(process.uptime()),
        totalCPF,
        totalConsultas,
      };
      res.status(200).json(response);
    })
    .catch(() => {
      response.payload = 'Ocorreu uma falha no servidor.';
      res.status(500).json(response);
    });
};
