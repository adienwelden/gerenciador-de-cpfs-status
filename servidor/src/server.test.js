import request from 'supertest';
import { assert } from 'chai';
import CPF from 'cpf-check';

import httpServer from './server';

let status = {
  uptime: null,
  totalCPF: 0,
  totalConsultas: 0,
};

// eslint-disable-next-line no-undef
describe('Teste da API:', () => {
  // eslint-disable-next-line no-undef
  it('Deve retornar o status do servidor', async () => {
    status = await request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        return response.body.payload;
      });
  });

  const cpfInvalido = '12345678901';

  // eslint-disable-next-line no-undef
  it('Deve retornar CPF inválido', () =>
    request(httpServer)
      .get(`/mmcpf/${cpfInvalido}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.msg,
          'CPF inválido',
          `Falha ao invalidar o CPF ${cpfInvalido}.`,
        );
      }));

  const cpf = CPF.generate();

  // eslint-disable-next-line no-undef
  it(`Deve retornar FREE para o CPF aleatório ${cpf}`, () =>
    request(httpServer)
      .get(`/mmcpf/${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'FREE',
          'Não retornou FREE como esperado.',
        );
      }));

  // eslint-disable-next-line no-undef
  it('Número de consultas deve estar incrementado de 1', () =>
    request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        assert.equal(
          response.body.payload.totalConsultas,
          status.totalConsultas + 1,
          'Número de consultas não está incrementado de 1.',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`Deve incluir o CPF ${cpf} na blacklist`, () =>
    request(httpServer)
      .post('/mmcpf')
      .send(`cpf=${cpf}`)
      .expect('Content-Type', /json/)
      .expect(201)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'inserido com sucesso',
          `CPF ${cpf} não foi incluído como deveria.`,
        );
      }));

  // eslint-disable-next-line no-undef
  it('Total de CPFs deve estar incrementado de 1', () =>
    request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        assert.equal(
          response.body.payload.totalCPF,
          status.totalCPF + 1,
          'Total de CPF não foi incrementado de 1.',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`O CPF ${cpf} deve estar na blacklist`, () =>
    request(httpServer)
      .get(`/mmcpf/${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'BLOCK',
          'CPF inserido não retornou BLOCK',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`Nova tentativa de incluir CPF existente (${cpf}) deve ser negada`, () =>
    request(httpServer)
      .post('/mmcpf')
      .send(`cpf=${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'já existe na lista suja',
          'Tentativa de inserir CPF existente não retornou como deveria.',
        );
      }));

  // eslint-disable-next-line no-undef
  it('Número de consultas deve estar incrementado de 2', () =>
    request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        assert.equal(
          response.body.payload.totalConsultas,
          status.totalConsultas + 2,
          'Número de consultas não foi incrementado de 2.',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`Deve remover o CPF ${cpf} e retornar removido da blacklist`, () =>
    request(httpServer)
      .delete(`/mmcpf/${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'removido com sucesso.',
          'Não retornou CPF removido com sucesso.',
        );
      }));

  // eslint-disable-next-line no-undef
  it('Total de CPFs deve ser o mesmo inicial', () =>
    request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        assert.equal(
          response.body.payload.totalCPF,
          status.totalCPF,
          'Número de CPFs não é o mesmo inicial.',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`O CPF ${cpf} NÂO deve estar na blacklist`, () =>
    request(httpServer)
      .get(`/mmcpf/${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'FREE',
          'CPF não retornou como liberado (FREE).',
        );
      }));

  // eslint-disable-next-line no-undef
  it('Número de consultas deve estar incrementado de 3', () =>
    request(httpServer)
      .get('/mmcpf/server/status')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.uptime,
          'Não retornou campo <uptime> na resposta.',
        );
        assert.exists(
          response.body.payload.totalCPF,
          'Não retornou campo <totalCPF> na resposta.',
        );
        assert.exists(
          response.body.payload.totalConsultas,
          'Não retornou campo <totalConsultas> na resposta.',
        );
        assert.equal(
          response.body.payload.totalConsultas,
          status.totalConsultas + 3,
          'Número de consultas não está incrementada de 3',
        );
      }));

  // eslint-disable-next-line no-undef
  it(`Tentativa de remoção de CPF inexistente ${cpf} deve retornar <não existe...>`, () =>
    request(httpServer)
      .delete(`/mmcpf/${cpf}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        assert.exists(response.body.ok, 'Não retornou campo <ok> na resposta.');
        assert.exists(
          response.body.payload.cpf,
          'Não retornou campo <cpf> na resposta.',
        );
        assert.exists(
          response.body.payload.msg,
          'Não retornou campo <msg> na resposta.',
        );
        assert.equal(
          response.body.payload.cpf,
          cpf,
          `Resposta não se refere ao CPF ${cpf} da requisição.`,
        );
        assert.equal(
          response.body.payload.msg,
          'NÃO existe na lista suja para que possa ser removido.',
          'Não retornou mensagem correta',
        );
      }));
});
