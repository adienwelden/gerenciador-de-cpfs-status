import mongoose from 'mongoose';

const cpfSchema = new mongoose.Schema({
  cpf: String,
});

const cpfs = mongoose.model('CPF', cpfSchema);

module.exports = cpfs;
