import http from 'http';
import Express from 'express';
import bodyParser from 'body-parser';
import swaggerUi from 'swagger-ui-express';

import swaggerDocument from '../swagger.json';
import cors from '../cors.json';
import serverConfig from '../config';
import cpfRoutes from './routes/cpf';

const app = new Express();

app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));

app.use((req, res, next) => {
  res.header(cors);
  next();
});

app.use('/mmcpf', cpfRoutes);
app.use('/api/v1', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const httpServer = http.createServer(app);

httpServer.listen(serverConfig.apiPort, () => {});

export default httpServer;
