const serverConfig = {
  apiHost: 'http://localhost',
  apiPort: process.env.PORT || 8463,
  mongodb: 'mongodb://localhost/mmcpf',
  firebaseDatabaseURL: '',
  firebaseStorageBucket: '',
  firebaseDatabaseName: '/mmcpf',
};

export default serverConfig;
