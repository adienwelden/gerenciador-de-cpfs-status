import React, { Component } from 'react';
import CPF from 'cpf-check';
import InputMask from 'react-input-mask';

import api from './uteis/api';

import Alerta from './alert';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resposta: { cpf: '', msg: '' },
      cpf: '',
      status: {
        uptime: 0,
        totalCPF: 0,
        totalConsultas: 0,
      },
      carregando: false,
    };
  }

  componentDidMount() {
    this.setState({ resposta: { cpf: '', msg: '' } });
    this.handleStatus();
  }

  handleCPF = event => {
    const cpf = event.target.value.trim();
    this.setState({ cpf, resposta: { cpf: '', msg: '' } });
  };

  cpfValido = () => {
    let { cpf } = this.state;
    cpf = CPF.strip(cpf);
    const resultado = CPF.validate(cpf);
    return resultado.valid;
  };

  handleInserir = () => {
    let { cpf } = this.state;
    if (this.cpfValido()) {
      this.setState({ carregando: true, resposta: { cpf: '', msg: '' } });
      cpf = CPF.strip(cpf);
      api('mmcpf', 'post', { cpf })
        .then(res => {
          if (res.ok) {
            this.setState({ carregando: false, resposta: res.payload });
          } else {
            this.setState({
              carregando: false,
              resposta: {
                cpf: CPF.format(cpf),
                msg: 'Ocorreu uma falha no servidor.',
              },
            });
          }
        })
        .then(() => api('mmcpf/server/status'))
        .then(res => {
          if (res.ok) this.setState({ carregando: false, status: res.payload });
          else this.setState({ carregando: false });
        })
        .catch(() => {
          this.setState({
            carregando: false,
            resposta: {
              cpf: '',
              msg: 'Ocorreu uma falha no servidor.',
            },
            status: {
              uptime: 0,
              totalCPF: 0,
              totalConsultas: 0,
            },
          });
        });
    } else {
      this.setState({
        resposta: { cpf, msg: 'O campo não contém um CPF válido.' },
      });
    }
  };

  handleConsultar = () => {
    let { cpf } = this.state;
    if (this.cpfValido()) {
      this.setState({ carregando: true, resposta: { cpf: '', msg: '' } });
      cpf = CPF.strip(cpf);
      api(`mmcpf/${cpf}`)
        .then(res => {
          if (res.ok) {
            const state = { carregando: false, resposta: res.payload };
            if (state.resposta.msg === 'FREE') {
              state.resposta.msg = 'NÂO existe na lista suja.';
            } else if (state.resposta.msg === 'BLOCK') {
              state.resposta.msg = 'existe na lista suja.';
            }
            this.setState(state);
          } else {
            this.setState({
              carregando: false,
              resposta: {
                cpf: CPF.format(cpf),
                msg: 'Ocorreu uma falha no servidor.',
              },
            });
          }
        })
        .then(() => api('mmcpf/server/status'))
        .then(res => {
          if (res.ok) this.setState({ carregando: false, status: res.payload });
          else this.setState({ carregando: false });
        })
        .catch(() => {
          this.setState({
            carregando: false,
            resposta: {
              cpf: '',
              msg: 'Ocorreu uma falha no servidor.',
            },
            status: {
              uptime: 0,
              totalCPF: 0,
              totalConsultas: 0,
            },
          });
        });
    } else {
      this.setState({
        resposta: { cpf, msg: 'O campo não contém um CPF válido.' },
      });
    }
  };

  handleRemover = () => {
    let { cpf } = this.state;
    if (this.cpfValido()) {
      this.setState({ carregando: true, resposta: { cpf: '', msg: '' } });
      cpf = CPF.strip(cpf);
      api(`mmcpf/${cpf}`, 'delete')
        .then(res => {
          if (res.ok) {
            this.setState({ carregando: false, resposta: res.payload });
          } else {
            this.setState({
              carregando: false,
              resposta: {
                cpf: CPF.format(cpf),
                msg: 'Ocorreu uma falha no servidor.',
              },
            });
          }
        })
        .then(() => api('mmcpf/server/status'))
        .then(res => {
          if (res.ok) this.setState({ carregando: false, status: res.payload });
          else this.setState({ carregando: false });
        })
        .catch(() => {
          this.setState({
            carregando: false,
            resposta: {
              cpf: '',
              msg: 'Ocorreu uma falha no servidor.',
            },
            status: {
              uptime: 0,
              totalCPF: 0,
              totalConsultas: 0,
            },
          });
        });
    } else {
      this.setState({
        resposta: { cpf, msg: 'O campo não contém um CPF válido.' },
      });
    }
  };

  handleGerar = () => {
    const cpf = CPF.generate();
    this.setState({
      cpf,
      carregando: false,
      resposta: { cpf: '', msg: '' },
    });
  };

  handleLimpar = () => {
    this.setState({
      cpf: '',
      carregando: false,
      resposta: { cpf: '', msg: '' },
    });
  };

  handleStatus = () => {
    api('mmcpf/server/status').then(res => {
      if (res.ok) this.setState({ carregando: false, status: res.payload });
    });
  };

  render() {
    const { cpf, resposta, status, carregando } = this.state;
    return (
      <div className="App">
        <h1> Gerencia lista suja de CPFs</h1>

        <form onSubmit={e => e.preventDefault()}>
          <div className="field">
            <div className="control has-icons-right">
              <InputMask
                name="cpf"
                id="cpf"
                placeholder="Entre com um CPF"
                className="input is-rounded"
                mask="999.999.999-99"
                maskChar={null}
                value={cpf}
                onChange={this.handleCPF}
              />
            </div>
          </div>

          <br />

          <Alerta
            msg={
              CPF.validate(resposta.cpf).valid
                ? `${CPF.format(resposta.cpf)}: ${resposta.msg}`
                : resposta.msg
            }
          />

          {carregando && <div className="loader" />}

          <br />
          <br />

          <div className="field">
            <div className="control">
              <button
                type="button"
                id="inserir"
                className="button is-link is-fullwidth"
                onClick={this.handleInserir}
              >
                Inserir
              </button>

              <button
                type="button"
                id="consultar"
                className="button is-link is-fullwidth"
                onClick={this.handleConsultar}
              >
                Consultar
              </button>

              <button
                type="button"
                id="remover"
                className="button is-link is-fullwidth"
                onClick={this.handleRemover}
              >
                Remover
              </button>

              <button
                type="button"
                id="gerar"
                className="button is-link is-fullwidth"
                onClick={this.handleGerar}
              >
                Gerar CPF
              </button>

              <button
                type="button"
                id="limpar"
                className="button is-link is-fullwidth"
                onClick={this.handleLimpar}
              >
                Limpar
              </button>
            </div>
          </div>

          <br />
          <br />

          <div className="field">
            <div className="control">
              <button
                type="button"
                id="status"
                className="button is-link is-fullwidth"
                onClick={this.handleStatus}
              >
                Atualizar status do servidor
              </button>
            </div>
          </div>

          <br />

          <div className="field">
            <span>Status do servidor:</span>
            <div className="control has-icons-right">
              <p>Uptime: {status.uptime}</p>
              <p>Total de CPFs cadastrados: {status.totalCPF}</p>
              <p>
                Total de consultas desde último restart: {status.totalConsultas}
              </p>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default App;
