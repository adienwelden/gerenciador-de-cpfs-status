import axios from 'axios';

import serverConfig from '../../../servidor/config';

export default function api(endpoint = '', method = 'get', body = {}) {
  const response = { ok: false, payload: null };

  const baseConfig = {
    url: `${serverConfig.apiHost}:${serverConfig.apiPort}/${endpoint}`,
    method,
    responseType: 'json',
    timeout: 5000,
  };

  // eslint-disable-next-line operator-linebreak
  const config =
    method === 'post'
      ? Object.assign({}, baseConfig, { data: body })
      : baseConfig;

  return axios(config)
    .then(res => res.data)
    .catch(() => {
      response.payload = {
        cpf: '',
        msg: 'Ocorreu uma falha no servidor.',
      };
      return response;
    });
}
