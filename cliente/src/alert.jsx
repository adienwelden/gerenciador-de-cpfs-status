import React from 'react';
import PropTypes from 'prop-types';

// eslint-disable-next-line no-confusing-arrow
const Alert = ({ msg }) =>
  msg ? <span className="tag is-warning is-medium">{msg}</span> : null;

Alert.defaultProps = {
  msg: '',
};

Alert.propTypes = {
  msg: PropTypes.string,
};

export default Alert;
