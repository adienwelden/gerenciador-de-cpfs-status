
## Funções com CPFs

- Gera
- Valida
- Insere
- Consulta
- Remove
- Lê Status do servidor

## Características

- Escolha entre Firebase e MongoDB
- Doc com Swagger `/api/v1`
- Testes com Jest `npm run test`
- Virtualização em Docker


## Rodar o projeto

Na pasta `servidor` e na pasta `cliente` do projeto:

```
$ npm install
$ npm start
```

## Dependências

- Node: `Node 8.16.0`.
- Framework do backend: `express`, `body-parser`, `nodemon`, `webpack`, `babel`.
- Recurso do Banco de Dados: `mongoDB`, `mongoose`, `firebase`.
- Requisições ao endpoint: `axios`.
- Testes: `jest`, `chai`, `supertest`.
- Framework do frontend: `react`, `webpack`, `babel`, `css-loader`.
- Validação e máscara do CPF: `cpf-check`, `react-input-mask`.
- Padronização código: `eslint`, `prettier`.

## Estrutura:

#### Servidor:

```
+-- servidor/
|	+-- src/
|	|	+-- api/
|	|	|	+-- mongodb.js
|	|	|	+-- firebase.js
|	|	+-- controllers/
|	|	|	+-- cpf.js
|	|	+-- models/
|	|	|	+-- cpfSchema.js
|	|	+-- routes/
|	|	|	+-- cpf.js
|	|	+-- server.js
|	|	+-- server.test.js
|	+-- .babelrc
|	+-- .dockerignore
|	+-- .editorconfig
|	+-- .eslintrc
|	+-- .prettierrc
|	+-- config.js
|	+-- cors.json
|	+-- Dockerfile
|	+-- firebaseKeys.json
|	+-- index.js
|	+-- jest.config.js
|	+-- package-lock.json
|	+-- package.json
|	+-- swagger.json
|	+-- webpack.config.js

```

#### Cliente:

```
+-- cliente/
|	+-- public/
|	|	+-- index.html
|	+-- src/
|	|	+-- uteis/
|	|	|	+-- api.js
|	|	+-- alert.js
|	|	+-- App.css
|	|	+-- App.js
|	+-- .babelrc
|	+-- .dockerignore
|	+-- .editorconfig
|	+-- .eslintrc
|	+-- .prettierrc
|	+-- Dockerfile
|	+-- package-lock.json
|	+-- package.json
|	+-- webpack.config.js

```
